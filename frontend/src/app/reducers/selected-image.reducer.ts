import { VerifiedImages } from '../models/verified-images.interface';

import * as SelectedImageActions from '../actions/selected-image.actions';

export function selectedImageReducer(state: VerifiedImages = null, action: SelectedImageActions.All) {
  switch (action.type) {
    case SelectedImageActions.SELECT:
      return action.payload;
    case SelectedImageActions.UNSELECT:
      return null;
    default:
      return state;
  }
}
