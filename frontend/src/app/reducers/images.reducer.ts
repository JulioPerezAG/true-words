import { FileCarousel } from '../models/file-carousel.interface';

import * as ImagesActions from '../actions/images.actions';

export function imagesReducer(state: FileCarousel[] = [], action: ImagesActions.All) {
  switch (action.type) {
    case ImagesActions.ADD:
      return [...state, action.payload];
    case ImagesActions.REMOVE:
      return state.filter((image: FileCarousel, index: number) => index !== action.payload);
    case ImagesActions.REMOVE_ALL:
      return [];
    default:
      return state;
  }
}
