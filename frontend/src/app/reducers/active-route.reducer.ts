import * as ActivateRouteActions from '../actions/active-route.actions';
import { LOAD } from '../actions/active-route.actions';

export function activeRouteReducer(state: string = '', action: ActivateRouteActions.All) {
  switch (action.type) {
    case LOAD:
      return action.route;
    default:
      return state;
  }
}
