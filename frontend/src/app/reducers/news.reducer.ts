import { News } from '../models/news.interface';
import { All, CLEAR, LOAD, LOAD_ID, UPDATE } from '../actions/news.actions';

export function newsReducer(state: News = {}, action: All) {
  switch (action.type) {
    case LOAD:
      return action.news;
    case LOAD_ID:
      return Object.assign(state, {id: action.id});
    case UPDATE:
      return {...state, ...action.news};
    case CLEAR:
      return {};
    default:
      return state;
  }
}
