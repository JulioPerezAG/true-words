import { All, BUY, EXPEND, LOAD } from '../actions/user-credits.actions';

export function userCreditsReducer(state = 0, action: All) {
  switch (action.type) {
    case LOAD:
      return action.credits;
    case BUY:
      return state + action.purchaseCredits;
    case EXPEND:
      return state - action.expendCredits;
    default:
      return state;
  }
}
