import { Action } from '@ngrx/store';

export const LOAD = '[User Credits] Load';

export const BUY = '[User Credits] Buy';

export const EXPEND = '[User Credits] Expend';

export class Load implements Action {
  readonly type = LOAD;

  constructor(public credits: number) {
  }
}

export class Buy implements Action {
  readonly type = BUY;

  constructor(public purchaseCredits: number) {
  }
}

export class Expend implements Action {
  readonly type = EXPEND;

  constructor(public expendCredits: number) {
  }
}

export type All = Load | Buy | Expend;
