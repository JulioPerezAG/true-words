import { Action } from '@ngrx/store';
import { News } from '../models/news.interface';

export const LOAD = '[News] Load';

export const LOAD_ID = '[News] Load Id';

export const UPDATE = '[News] Update';

export const CLEAR = '[News] Clear';

export class Load implements Action {
  readonly type = LOAD;

  constructor(public news: News) {
  }
}

export class LoadId implements Action {
  readonly type = LOAD_ID;

  constructor(public id: string) {
  }
}

export class Update implements Action {
  readonly type = UPDATE;

  constructor(public news: News) {
  }
}

export class Clear implements Action {
  readonly type = CLEAR;
}

export type All = Load | LoadId | Update | Clear;
