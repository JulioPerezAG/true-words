import { Action } from '@ngrx/store';

export const LOAD = '[Router] Load';

export class Load implements Action {
  readonly type = LOAD;

  constructor(public route: string) {
  }
}

export type All = Load;
