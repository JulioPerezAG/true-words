import { Action } from '@ngrx/store';
import { FileCarousel } from '../models/file-carousel.interface';

export const ADD = '[Images] Add';

export const REMOVE = '[Images] Remove';

export const REMOVE_ALL = '[Images] Remove All';

export class Add implements Action {
  readonly type = ADD;

  constructor(public payload: FileCarousel) {
  }
}

export class Remove implements Action {
  readonly type = REMOVE;

  constructor(public payload: number) {
  }
}

export class RemoveAll implements Action {
  readonly type = REMOVE_ALL;
}

export type All = Add | Remove | RemoveAll;
