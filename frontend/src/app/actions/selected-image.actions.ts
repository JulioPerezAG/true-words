import { Action } from '@ngrx/store';
import { VerifiedImages } from '../models/verified-images.interface';

export const SELECT = '[Selected Images] Select';

export const UNSELECT = '[Selected Images] UnSelect';

export class Select implements Action {
  readonly type = SELECT;

  constructor(public payload: VerifiedImages) {
  }
}

export class UnSelect implements Action {
  readonly type = UNSELECT;
}

export type All = Select | UnSelect;
