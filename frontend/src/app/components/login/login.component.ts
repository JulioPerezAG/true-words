import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { LoginForm } from '../../models/login-form.interface';

import { faSpinner } from '@fortawesome/free-solid-svg-icons/faSpinner';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss', '../../../assets/styles/true-words-forms.scss']
})
export class LoginComponent {

  @Input() loading: boolean;

  @Output() attemptLogin: EventEmitter<LoginForm>;

  @Output() attemptSignUp: EventEmitter<any>;

  form: FormGroup;

  loadingIcon = faSpinner;

  constructor(formBuilder: FormBuilder) {
    this.form = formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      rememberMe: false
    });

    this.attemptLogin = new EventEmitter<LoginForm>();
    this.attemptSignUp = new EventEmitter<any>();
  }

  onAttemptLogin() {
    this.attemptLogin.emit(this.form.value);
  }

  onAttemptSignUp() {
    this.attemptSignUp.emit();
  }
}
