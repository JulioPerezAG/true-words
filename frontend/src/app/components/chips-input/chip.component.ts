import { Component, EventEmitter, Input, Output } from '@angular/core';

import { faTimes } from '@fortawesome/free-solid-svg-icons/faTimes';

@Component({
  selector: 'app-chip',
  templateUrl: './chip.component.html',
  styleUrls: ['./chips-input.component.scss']
})
export class ChipComponent {

  @Input() text: string;

  @Input() index: number;

  @Output() deleteChip: EventEmitter<number>;

  close = faTimes;

  constructor() {
    this.deleteChip = new EventEmitter<number>();
  }

  onDelete() {
    this.deleteChip.emit(this.index);
  }
}
