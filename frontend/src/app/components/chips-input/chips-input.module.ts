import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { ChipsInputComponent } from './chips-input.component';
import { ChipComponent } from './chip.component';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [
    ChipsInputComponent,
    ChipComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FontAwesomeModule
  ],
  exports: [
    ChipsInputComponent
  ]
})
export class ChipsInputModule {

}
