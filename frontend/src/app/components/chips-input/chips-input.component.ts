import { Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';

import { faPlus } from '@fortawesome/free-solid-svg-icons/faPlus';

@Component({
  selector: 'app-chips-input',
  templateUrl: './chips-input.component.html',
  styleUrls: [
    './chips-input.component.scss',
    '../../../assets/styles/true-words-forms.scss'
  ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ChipsInputComponent),
      multi: true
    }
  ]
})
export class ChipsInputComponent implements ControlValueAccessor {

  @Input() placeHolder: string;

  form: FormGroup;

  change: Function;

  chips: string[];

  add = faPlus;

  constructor(formBuilder: FormBuilder) {
    this.form = formBuilder.group({chipInput: ['', Validators.required]});
    this.chips = [];
  }

  onAddedChip() {
    this.chips.push(this.form.controls.chipInput.value);
    this.form.reset();
    if (this.change) {
      this.change(this.chips);
    }
  }

  onDeleteChip(i: number) {
    this.chips = this.chips.filter((value, index) => index !== i);
    if (this.change) {
      this.change(this.chips);
    }
  }

  registerOnChange(fn: Function): void {
    this.change = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState(isDisabled: boolean): void {
    this.form[isDisabled ? 'disable' : 'enable']();
  }

  writeValue(obj: string[]): void {
    if (obj) {
      this.chips = obj;
    } else {
      this.chips = [];
    }
  }
}
