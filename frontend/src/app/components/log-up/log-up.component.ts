import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { PasswordValidators } from '../../validators/password/password.validators';

import { faSpinner } from '@fortawesome/free-solid-svg-icons/faSpinner';

import { LogUpForm } from '../../models/log-up-form.interface';

@Component({
  selector: 'app-log-up',
  templateUrl: './log-up.component.html',
  styleUrls: ['./log-up.component.scss',
    '../../../assets/styles/true-words-forms.scss']
})
export class LogUpComponent implements OnInit {

  @Input() loading: boolean;

  @Output() size: EventEmitter<string>;

  @Output() attemptLogUp: EventEmitter<LogUpForm>;

  loadingIcon = faSpinner;

  form: FormGroup;

  constructor(formBuilder: FormBuilder) {
    this.form = formBuilder.group({
      name: ['', Validators.required],
      firstLastName: ['', Validators.required],
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required],
      company: ['']
    }, {
      validator: PasswordValidators
        .matchPassword('password', 'confirmPassword')
    });

    this.size = new EventEmitter<string>();
    this.attemptLogUp = new EventEmitter<LogUpForm>();
  }

  ngOnInit(): void {
    this.size.emit('sm');
  }

  onAttemptLogUp() {
    this.attemptLogUp.emit(this.form.value);
  }
}
