import { Component, EventEmitter, Input, Output, TemplateRef } from '@angular/core';

import { faChevronLeft } from '@fortawesome/free-solid-svg-icons/faChevronLeft';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss',
    '../../../assets/styles/header.scss']
})
export class LayoutComponent {

  @Input() templateOutlet: TemplateRef<any>;

  @Input() context: Object;

  @Input() username: string;

  @Input() email: string;

  @Input() selectedScreen: string;

  @Input() loading: boolean;

  @Output() logout: EventEmitter<void>;

  @Output() changeSelectedScreen: EventEmitter<string>;

  @Output() goBack: EventEmitter<void>;

  @Output() openCreditsRequestModal: EventEmitter<void>;

  _fromCredits = 0;

  back = faChevronLeft;

  countTo: number;

  constructor() {
    this.logout = new EventEmitter();
    this.changeSelectedScreen = new EventEmitter();
    this.goBack = new EventEmitter();
    this.openCreditsRequestModal = new EventEmitter();
  }

  _credits = 0;

  @Input() set credits(credits: number) {
    this._fromCredits = this._credits;
    this._credits = credits;
  }

  onChangeSelectedScreen(screen: string) {
    this.changeSelectedScreen.emit(screen);
  }

  onGoBack() {
    this.goBack.emit();
  }

  onOpenCreditsRequestModal() {
    this.openCreditsRequestModal.emit();
  }

  onLogout() {
    this.logout.emit();
  }
}
