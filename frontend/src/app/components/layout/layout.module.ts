import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutComponent } from './layout.component';

import { IntrusiveLoadingModule } from '../../features/intrusive-loading/intrusive-loading.module';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

import { CountoModule } from 'angular2-counto';

@NgModule({
  declarations: [
    LayoutComponent
  ],
  imports: [
    CommonModule,
    IntrusiveLoadingModule,
    FontAwesomeModule,
    NgbDropdownModule,
    CountoModule
  ],
  exports: [
    LayoutComponent
  ]
})
export class LayoutModule {

}
