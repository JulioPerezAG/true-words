import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { faPlus } from '@fortawesome/free-solid-svg-icons/faPlus';
import { faMinus } from '@fortawesome/free-solid-svg-icons/faMinus';

@Component({
  selector: 'app-credits-request-modal',
  templateUrl: './credits-request-modal.component.html',
  styleUrls: ['./credits-request-modal.component.scss',
    '../../../assets/styles/true-words-buttons.scss']
})
export class CreditsRequestModalComponent {

  plus = faPlus;

  minus = faMinus;

  form: FormGroup;

  constructor(formBuilder: FormBuilder,
              public activeModal: NgbActiveModal) {
    this.form = formBuilder.group({credits: ['0', Validators.required]});
  }

  more() {
    this.form.controls.credits.patchValue(Number.parseInt(this.form.value.credits) + 1);
  }

  less() {
    this.form.controls.credits.patchValue(Number.parseInt(this.form.value.credits) - 1);
  }

  request() {
    this.activeModal.close(this.form.value);
  }
}
