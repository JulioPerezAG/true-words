import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { CreditsRequestModalComponent } from './credits-request-modal.component';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [
    CreditsRequestModalComponent
  ],
  imports: [
    ReactiveFormsModule,
    FontAwesomeModule
  ],
  exports: [
    CreditsRequestModalComponent
  ],
  entryComponents: [
    CreditsRequestModalComponent
  ]
})
export class CreditsRequestModalModule {
}
