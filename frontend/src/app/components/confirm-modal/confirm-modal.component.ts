import { Component, EventEmitter, Output } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss']
})
export class ConfirmModalComponent {

  @Output() confirm: EventEmitter<Event>;

  @Output() reject: EventEmitter<Event>;

  action = 'publicar la noticia';

  constructor(private activeModal: NgbActiveModal) {
    this.confirm = new EventEmitter();
    this.reject = new EventEmitter();
  }

  onConfirm(event?: Event) {
    this.confirm.emit(event);
    this.activeModal.close('Ok');
  }

  onReject(event?: Event) {
    this.reject.emit(event);
    this.activeModal.dismiss('Cancel');
  }

  onClose() {
    this.onReject();
  }
}
