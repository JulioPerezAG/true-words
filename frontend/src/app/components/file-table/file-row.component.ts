import { Component, Input } from '@angular/core';

import { VerifiedImage } from '../../models/verified-image.interface';

@Component({
  selector: '[app-file-row]',
  templateUrl: './file-row.component.html',
  styleUrls: ['./file-row.component.scss']
})
export class FileRowComponent {

  @Input('app-file-row') data: VerifiedImage;

  @Input() stringMaxLength = 30;

  @Input() lastItem: boolean;
}
