import { Component, Input } from '@angular/core';

import { VerifiedImages } from '../../models/verified-images.interface';

@Component({
  selector: 'app-file-table',
  templateUrl: './file-table.component.html',
  styleUrls: ['./file-table.component.scss']
})
export class FileTableComponent {

  @Input() data: VerifiedImages;
}
