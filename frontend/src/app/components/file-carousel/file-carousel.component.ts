import { Component, ElementRef, EventEmitter, Input, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';

import { FileCarouselItemComponent } from './file-carousel-item.component';

import { faArrowLeft } from '@fortawesome/free-solid-svg-icons/faArrowLeft';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons/faArrowRight';

import { FileCarousel } from '../../models/file-carousel.interface';

@Component({
  selector: 'app-file-carousel',
  templateUrl: './file-carousel.component.html',
  styleUrls: ['./file-carousel.component.scss']
})
export class FileCarouselComponent {

  @Input() images: FileCarousel[];

  @Input() selected = 0;

  @Input() carouselSpeed = 3;

  @Output() selectedFile: EventEmitter<number>;

  @Output() deleteFile: EventEmitter<number>;

  @ViewChild('stageSF') stageSF: ElementRef;

  stageWidth = 0;

  stageWidthF = 0;

  stageOuterWidth = 0;

  positionX = 0;

  arrowLeft = faArrowLeft;

  arrowRight = faArrowRight;

  constructor() {
    this.selectedFile = new EventEmitter();
    this.deleteFile = new EventEmitter();
  }

  _stageOuter: ElementRef;

  @ViewChild('stageOuter') set stageOuter(stageOuter: ElementRef) {
    this._stageOuter = stageOuter;
  }

  _items: QueryList<FileCarouselItemComponent>;

  @ViewChildren(FileCarouselItemComponent) set items(items: QueryList<FileCarouselItemComponent>) {
    this._items = items;
    this.onResizeStage();
  }

  onDeleteItem(item: number) {
    this.deleteFile.emit(item);
  }

  onClick(item: number) {
    this.selectedFile.emit(item);
    this.selected = item;
    this.onResizeStage();
  }

  onDragOnX(x: number) {
    x < 0 ? this.onNext() : this.onPrev();
  }

  onChangeSize() {
    this.onResizeStage();
  }

  onResizeStage() {
    setTimeout(() => {
      this.stageWidthF = this.stageSF.nativeElement.offsetWidth;
      this.stageOuterWidth = this._stageOuter.nativeElement.offsetWidth;
      this.stageWidth = this._items.reduce((prevValue: number, curValue: FileCarouselItemComponent) => prevValue + curValue.width, 0)
        + this._items.length * 16.97;
    });
  }

  onNext() {
    if (this.positionX > this.stageOuterWidth - this.stageWidthF) {
      this.positionX -= this.carouselSpeed;
    }
  }

  onPrev() {
    if (this.positionX < 0) {
      this.positionX += this.carouselSpeed;
      if (this.positionX > 0) {
        this.positionX = 0;
      }
    }
  }
}
