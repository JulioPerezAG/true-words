import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-activity-button',
  templateUrl: './activity-button.component.html',
  styleUrls: ['./activity-button.component.scss']
})
export class ActivityButtonComponent {

  @Input() title: string;

  @Input() value = 0;

  @Input() bgColor = '#dce2eb';

  @Input() textColor = '#153f79';

  @Input() selected: boolean;

  activated: boolean;
}
