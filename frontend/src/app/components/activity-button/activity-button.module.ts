import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ActivityButtonComponent } from './activity-button.component';

@NgModule({
  declarations: [
    ActivityButtonComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ActivityButtonComponent
  ]
})
export class ActivityButtonModule {
}
