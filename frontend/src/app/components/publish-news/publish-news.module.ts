import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { PublishNewsComponent } from './publish-news.component';

import { FileCarouselModule } from '../file-carousel/file-carousel.module';

@NgModule({
  declarations: [
    PublishNewsComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FileCarouselModule,
    FileCarouselModule
  ],
  exports: [
    PublishNewsComponent
  ]
})
export class PublishNewsModule {
}
