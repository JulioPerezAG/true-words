import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FileCarousel } from '../../models/file-carousel.interface';
import { News } from '../../models/news.interface';

import * as _ from 'lodash';

@Component({
  selector: 'app-publish-news',
  templateUrl: './publish-news.component.html',
  styleUrls: ['./publish-news.component.scss',
    '../../../assets/styles/true-words-forms.scss',
    '../../../assets/styles/true-words-buttons.scss']
})
export class PublishNewsComponent implements OnInit {

  @Input() images: FileCarousel[];

  @Input() news: News;

  selectedImageIndex: number;

  form: FormGroup;

  @Output() deleteFile: EventEmitter<number>;

  @Output() publishNews: EventEmitter<News>;

  @Output() clear: EventEmitter<any>;

  constructor(formBuilder: FormBuilder) {
    this.form = formBuilder.group({
      news: [{value: '', disabled: true}, Validators.required],
      url: ['', Validators.required]
    });

    this.deleteFile = new EventEmitter();
    this.publishNews = new EventEmitter();
    this.clear = new EventEmitter();
  }

  ngOnInit(): void {
    this.form.patchValue(this.news);
  }

  onDelete(item: number) {
    this.deleteFile.emit(item);
  }

  onPublish() {
    this.publishNews.emit({...this.news, ...this.form.value});
  }

  clearNews() {
    this.clear.emit();
  }

  isEmpty(a: any): boolean {
    return _.isEmpty(a);
  }
}
