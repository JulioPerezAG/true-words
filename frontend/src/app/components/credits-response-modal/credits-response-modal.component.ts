import { Component } from '@angular/core';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-credits-response-modal',
  templateUrl: './credits-response-modal.component.html',
  styleUrls: ['./credits-response-modal.component.scss',
    '../../../assets/styles/true-words-buttons.scss']
})
export class CreditsResponseModalComponent {

  credits: number;

  constructor(public activeModal: NgbActiveModal) {
  }
}
