import { NgModule } from '@angular/core';

import { CreditsResponseModalComponent } from './credits-response-modal.component';

@NgModule({
  declarations: [
    CreditsResponseModalComponent
  ],
  imports: [],
  exports: [
    CreditsResponseModalComponent
  ],
  entryComponents: [
    CreditsResponseModalComponent
  ]
})
export class CreditsResponseModalModule {
}
