import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { EditorComponent } from './editor.component';

@NgModule({
  declarations: [
    EditorComponent
  ],
  imports: [
    ReactiveFormsModule,
    FontAwesomeModule
  ],
  exports: [
    EditorComponent
  ]
})
export class EditorModule {

}
