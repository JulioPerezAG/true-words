import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';

import { faPlay } from '@fortawesome/free-solid-svg-icons/faPlay';
import { faFont } from '@fortawesome/free-solid-svg-icons/faFont';
import { faCamera } from '@fortawesome/free-solid-svg-icons/faCamera';
import { faTrashAlt } from '@fortawesome/free-regular-svg-icons/faTrashAlt';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: [
    './editor.component.scss',
    '../../../assets/styles/true-words-forms.scss'
  ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => EditorComponent),
      multi: true
    }
  ]
})
export class EditorComponent implements ControlValueAccessor, OnInit {

  @Input() placeHolder = '';

  form: FormGroup;

  change: Function;

  play = faPlay;

  font = faFont;

  camera = faCamera;

  trash = faTrashAlt;

  constructor(formBuilder: FormBuilder) {
    this.form = formBuilder.group({editor: ''});
  }

  ngOnInit(): void {
    this.form.controls.editor.valueChanges.subscribe(value => this.change ? this.change(value) : '');
  }

  registerOnChange(fn: Function): void {
    this.change = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState(isDisabled: boolean): void {
    this.form[isDisabled ? 'disable' : 'enable']();
  }

  writeValue(obj: string): void {
    this.form.controls.editor.patchValue(obj);
    if (this.change) {
      this.change(obj);
    }
  }
}
