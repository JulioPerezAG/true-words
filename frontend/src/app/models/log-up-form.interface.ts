export interface LogUpForm {
  name: string;
  firstLastName: string;
  secondLastName: string;
  username: string;
  email: string;
  password: string;
  token: string;
}
