export interface News {
  text?: string;
  description?: string;
  images?: string[];
  tags?: string[];
  url?: string;
  id?: string;
}
