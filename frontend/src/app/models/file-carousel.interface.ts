import { VerifiedImages } from './verified-images.interface';

export interface FileCarousel {
  data: VerifiedImages;
  image: string;
  url: string;
}
