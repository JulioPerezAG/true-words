import { VerifiedImage } from './verified-image.interface';

export interface VerifiedImages {
  data?: Data;
}

interface Data {
  fullMatchingImages?: VerifiedImage[];
  partialMatchingImages?: VerifiedImage[];
}
