export interface CreditsToBuy {
  quantity: number;
  userEmail: string;
}
