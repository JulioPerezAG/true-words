import { FileCarousel } from './file-carousel.interface';
import { VerifiedImages } from './verified-images.interface';
import { News } from './news.interface';

export interface AppState {
  intrusiveLoading: boolean;
  images: FileCarousel[];
  selectedImage: VerifiedImages;
  activeRoute: string;
  userCredits: number;
  news: News;
}
