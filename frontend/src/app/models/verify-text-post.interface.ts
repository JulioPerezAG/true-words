import { News } from './news.interface';

export interface VerifyTextPost {
  news: News;
  userEmail: string;
}
