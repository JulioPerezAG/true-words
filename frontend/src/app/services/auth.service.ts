import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { LoginForm } from '../models/login-form.interface';
import { LogUpForm } from '../models/log-up-form.interface';

import { environment } from '../../environments/environment';

@Injectable()
export class AuthService {

  uri: string;

  constructor(private http: HttpClient) {
    this.uri = `${environment.apiUri}/auth`;
  }

  login(loginForm: LoginForm): Observable<any> {
    return this.http.post(`${this.uri}/login`, loginForm);
  }

  logUp(logUpForm: LogUpForm): Observable<any> {
    return this.http.post(`${this.uri}/signup`, logUpForm);
  }
}
