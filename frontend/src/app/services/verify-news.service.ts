import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';

import { VerifyTextPost } from '../models/verify-text-post.interface';

@Injectable()
export class VerifyNewsService {

  uri: string;

  constructor(private http: HttpClient) {
    this.uri = `${environment.apiUri}/news`;
  }

  verifyNews(data: VerifyTextPost): Observable<any> {
    return this.http.post(`${this.uri}/verify`, data);
  }
}
