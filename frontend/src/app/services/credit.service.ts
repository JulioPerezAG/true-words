import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { CreditsToBuy } from '../models/credits-to-buy.interface';
import { Observable } from 'rxjs';
import { CreditsInterface } from '../models/credits.interface';

@Injectable()
export class CreditService {
    uri: string;

    constructor(private http: HttpClient) {
        this.uri = `${environment.apiUri}/credits`;
    }

    buyCredits(creditsToBuy: CreditsToBuy): Observable<any> {
        return this.http.put(`${this.uri}/buy-credits`, creditsToBuy);
    }

    getUserCredits(userEmail: string): Observable<CreditsInterface> {
        let headers = new HttpHeaders();
        headers = headers.set('userEmail', userEmail);
        return this.http.get<CreditsInterface>(`${this.uri}`, { headers });
    }
}
