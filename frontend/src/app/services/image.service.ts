import { Injectable } from '@angular/core';

import { Observable, Observer } from 'rxjs';

@Injectable()
export class ImageService {

  imageToBase64(file: File): Observable<string | ArrayBuffer> {
    return Observable.create((observer: Observer<string | ArrayBuffer>) => {
      const reader = new FileReader();
      reader.onloadend = () =>
        observer.next(reader.result);
      reader.onerror = () =>
        observer.error(reader.error);
      reader.readAsDataURL(file);
    });
  }
}
