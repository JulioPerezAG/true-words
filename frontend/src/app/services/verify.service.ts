import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';

import { Observable, Observer } from 'rxjs';

import { VerifyImagePost } from '../models/verify-image-post.interface';
import { VerifiedImages } from '../models/verified-images.interface';

import * as firebase from 'firebase';
import { FirebaseTaskStatus } from '../models/firebase-task-status.interface';
import Reference = firebase.storage.Reference;
import UploadTask = firebase.storage.UploadTask;
import UploadTaskSnapshot = firebase.storage.UploadTaskSnapshot;

@Injectable()
export class VerifyService {

  uri: string;

  storageRef: Reference;

  constructor(private http: HttpClient) {
    this.uri = `${environment.apiUri}/image`;
    this.storageRef = firebase.storage().ref();
  }

  verifyImage(data: VerifyImagePost): Observable<VerifiedImages> {
    return this.http.post<VerifiedImages>(`${this.uri}/upload`, data);
  }

  uploadImage(file: File): Observable<FirebaseTaskStatus> {
    return Observable.create((observer: Observer<FirebaseTaskStatus>) => {
      const uploadTask: UploadTask = this.storageRef.child(new Date().getTime().toString()).put(file);
      uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
        (snapshot: UploadTaskSnapshot) => {
          const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          const state = snapshot.state;
          console.log(`Upload is ${state}, ${progress} % done`);
          observer.next({status: state, progress: progress});
        }, (error: any) => observer.error(error.code),
        () => uploadTask.snapshot.ref.getDownloadURL().then(url =>
          observer.next({status: 'COMPLETE', downloadUrl: url})));
    });
  }
}
