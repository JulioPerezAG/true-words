import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class CurrentCreditService {
    private creditSource = new BehaviorSubject(0);
    currentCredit = this.creditSource.asObservable();
    constructor() { }

    changeCredit(credits: number) {
        this.creditSource.next(credits + this.creditSource.value);
    }

    removeCredits() {
        this.creditSource.next(0);
    }

}
