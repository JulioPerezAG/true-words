import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class NewsService {

  uri: string;

  constructor(private httpClient: HttpClient) {
    this.uri = `/api/news`;
  }

  partialSave(userEmail: string, news: any): Observable<any> {
    return this.httpClient.post(`${this.uri}/partial-save`, {userEmail, news});
  }

  saveNews(userEmail: string, news: any): Observable<any> {
    return this.httpClient.post(`${this.uri}/save-news`, {userEmail, news});
  }

  deletePartial(userEmail: string): Observable<any> {
    return Observable.create(s => {
      s.onNext();
    });
  }
}
