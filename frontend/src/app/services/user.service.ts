import { Injectable } from '@angular/core';
import { User } from 'firebase';

@Injectable()
export class UserService {

  get user(): User {
    let _user = JSON.parse(localStorage.getItem('user'));
    if (_user) {
      _user = _user.data;
    }
    if (_user) {
      _user = _user.user;
    }
    return _user;
  }

  set user(user: User) {
    localStorage.setItem('user', JSON.stringify(user));
  }

  removeUser() {
    localStorage.clear();
  }
}
