import { ExtraOptions, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

export const routes: Routes = [
  {path: 'login', loadChildren: './views/login-view/login-view.module#LoginViewModule', canLoad: [AuthGuard]},
  {path: 'home', loadChildren: './views/layout-view/layout-view.module#LayoutViewModule', canLoad: [AuthGuard]},
  {path: '', pathMatch: 'full', redirectTo: 'login'}];

export const options: ExtraOptions = {useHash: true};
