import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';

import { options, routes } from './app.routes';

import { AuthGuard } from './guards/auth.guard';

import { UserService } from './services/user.service';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ToastrModule } from 'ngx-toastr';

import * as firebase from 'firebase';

import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { imagesReducer } from './reducers/images.reducer';
import { selectedImageReducer } from './reducers/selected-image.reducer';
import { activeRouteReducer } from './reducers/active-route.reducer';
import { userCreditsReducer } from './reducers/user-credits.reducer';
import { newsReducer } from './reducers/news.reducer';
import { NewsService } from './services/news.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes, options),
    HttpClientModule,
    NgbModule.forRoot(),
    ToastrModule.forRoot(),
    StoreModule.forRoot({
      images: imagesReducer,
      selectedImage: selectedImageReducer,
      activeRoute: activeRouteReducer,
      userCredits: userCreditsReducer,
      news: newsReducer
    }),
    StoreDevtoolsModule.instrument({maxAge: 20})
  ],
  providers: [
    AuthGuard,
    UserService,
    NewsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  config = {
    apiKey: 'AIzaSyBkPfC_TANshpe234GAkzHsjebcXPHqIsI',
    storageBucket: 'fake-news-v2.appspot.com'
  };

  constructor() {
    firebase.initializeApp(this.config);
  }
}
