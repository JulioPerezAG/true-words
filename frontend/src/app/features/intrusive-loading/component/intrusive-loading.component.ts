import { Component, Input } from '@angular/core';
import { faSpinner } from '@fortawesome/free-solid-svg-icons/faSpinner';

@Component({
  selector: 'app-intrusive-loading',
  templateUrl: './intrusive-loading.component.html',
  styleUrls: ['./intrusive-loading.component.scss']
})
export class IntrusiveLoadingComponent {

  @Input() disposable: boolean;

  @Input() loading: boolean;

  @Input() message: string;

  spinner = faSpinner;

  onClick() {
    if (this.disposable) {
      this.loading = false;
    }
  }
}
