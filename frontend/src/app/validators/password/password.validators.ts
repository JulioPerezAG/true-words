import { AbstractControl, ValidationErrors } from '@angular/forms';

export class PasswordValidators {

  public static matchPassword(passwordControlName: string, confirmPasswordControlName: string): Function {
    return (abstractControl: AbstractControl): ValidationErrors | null => {
      const password = abstractControl.get(passwordControlName),
        confirmPassword = abstractControl.get(confirmPasswordControlName);
      if ((!password || !confirmPassword) || (password.value !== confirmPassword.value)) {
        return {matchPassword: true};
      } else {
        return null;
      }
    };
  }
}
