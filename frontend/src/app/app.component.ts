import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';

import { Store } from '@ngrx/store';
import * as ActiveRouteActions from './actions/active-route.actions';

import { AppState } from './models/app-state.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  subscriptions: Subscription;

  constructor(private router: Router,
              private store: Store<AppState>) {
    this.subscriptions = new Subscription();
  }

  ngOnInit(): void {
    this.subscriptions.add(this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd) => this.store.dispatch(new ActiveRouteActions.Load(event.urlAfterRedirects))));
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
