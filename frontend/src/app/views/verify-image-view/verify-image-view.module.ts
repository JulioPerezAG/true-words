import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VerifyImageViewComponent } from './verify-image-view.component';

import { VerifyService } from '../../services/verify.service';
import { ImageService } from '../../services/image.service';

import { FileChooserDragNDropModule } from '../../components/file-chooser-drag-n-drop/file-chooser-drag-n-drop.module';
import { FileTableModule } from '../../components/file-table/file-table.module';
import { FileCarouselModule } from '../../components/file-carousel/file-carousel.module';

@NgModule({
  declarations: [
    VerifyImageViewComponent
  ],
  imports: [
    CommonModule,
    FileChooserDragNDropModule,
    FileTableModule,
    FileCarouselModule
  ],
  exports: [
    VerifyImageViewComponent
  ],
  providers: [
    VerifyService,
    ImageService
  ]
})
export class VerifyImageViewModule {

}
