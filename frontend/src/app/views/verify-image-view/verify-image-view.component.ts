import { Component, OnInit } from '@angular/core';

import { FileCarousel } from '../../models/file-carousel.interface';
import { VerifiedImages } from '../../models/verified-images.interface';

import { VerifyService } from '../../services/verify.service';
import { ImageService } from '../../services/image.service';

import { filter, finalize, map } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { FirebaseTaskStatus } from '../../models/firebase-task-status.interface';
import { AppState } from '../../models/app-state.interface';

import { select, Store } from '@ngrx/store';

import * as UserCreditsActions from '../../actions/user-credits.actions';
import * as ImagesActions from '../../actions/images.actions';
import * as SelectedImageActions from '../../actions/selected-image.actions';

import { ToastrService } from 'ngx-toastr';
import { NewsService } from '../../services/news.service';
import { UserService } from '../../services/user.service';

import * as NewsActions from '../../actions/news.actions';

@Component({
  selector: 'app-verify-image-view',
  templateUrl: './verify-image-view.component.html',
  styleUrls: ['./verify-image-view.component.scss']
})
export class VerifyImageViewComponent implements OnInit {

  images: Observable<FileCarousel[]>;

  selectedImage: Observable<VerifiedImages>;

  loading: boolean;

  selectedImageIndex: number;

  chooserError: boolean;

  email: string;

  constructor(private verifyService: VerifyService,
              private imageService: ImageService,
              private store: Store<AppState>,
              private toastr: ToastrService,
              private newsService: NewsService,
              private userService: UserService) {
  }

  ngOnInit(): void {
    this.images = this.store.pipe(select(state => state.images));
    this.selectedImage = this.store.pipe(select(state => state.selectedImage));
    this.images.subscribe(value => {
      if (value.length) {
        this.onChooseImage(value.length - 1, value);
        this.selectedImageIndex = value.length - 1;
      }
    });
    this.email = this.userService.user.email;
  }

  onDelete(item: number) {
    this.store.dispatch(new ImagesActions.Remove(item));
    this.selectedImage = null;
  }

  onChooseFile(file: File) {
    this.loading = true;
    this.verifyService.uploadImage(file)
      .pipe(filter((value: FirebaseTaskStatus) => value.status === 'COMPLETE'))
      .subscribe(status =>
        this.imageService.imageToBase64(file)
          .pipe(map(value => typeof value === 'string' ? value : value.toString()))
          .subscribe(image =>
              this.verifyService.verifyImage({
                imageUrl: status.downloadUrl
              }).pipe(finalize(() => this.loading = false))
                .subscribe(next => {
                    this.store.dispatch(new ImagesActions.Add({data: next, image: image, url: status.downloadUrl}));
                    this.store.dispatch(new UserCreditsActions.Expend(1));
                  },
                  () => this.displayChooserError()),
            () => {
              this.loading = false;
              this.displayChooserError();
            }), () => {
        this.loading = false;
        this.displayChooserError();
      });
  }

  onChooseImage(index: number, images?: FileCarousel[]) {
    if (images) {
      this.store.dispatch(new SelectedImageActions.Select(images[index].data));
    } else {
      this.images.subscribe(value => this.store.dispatch(new SelectedImageActions.Select(value[index].data)));
    }
  }

  displayChooserError() {
    this.chooserError = true;
    this.toastr.error('Ocurrio un error mientras se subia la imagen');
    setTimeout(() => this.chooserError = false, 1000);
  }

  onSave() {
    this.images.subscribe(images => this.newsService.partialSave(this.email, {images: images})
      .subscribe(id => this.store.dispatch(new NewsActions.LoadId(id))));
  }
}
