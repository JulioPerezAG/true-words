import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

import { UserService } from '../../services/user.service';
import { CreditService } from '../../services/credit.service';

import { select, Store } from '@ngrx/store';

import { Observable, Subscription } from 'rxjs';
import { fromPromise } from 'rxjs/internal-compatibility';
import { finalize, map } from 'rxjs/operators';

import { AppState } from '../../models/app-state.interface';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { CreditsRequestModalComponent } from '../../components/credits-request-modal/credits-request-modal.component';
import { CreditsResponseModalComponent } from '../../components/credits-response-modal/credits-response-modal.component';

import { User } from 'firebase';

import { Buy, Load } from '../../actions/user-credits.actions';

@Component({
  selector: 'app-layout-view',
  templateUrl: './layout-view.component.html'
})
export class LayoutViewComponent implements OnInit, OnDestroy {

  view: Observable<string>;

  user: User;

  loading: boolean;

  credits: Observable<number>;

  subscriptions: Subscription;

  constructor(private activateRoute: ActivatedRoute,
              private router: Router,
              private location: Location,
              private modalService: NgbModal,
              private userService: UserService,
              private creditService: CreditService,
              private store: Store<AppState>) {
    this.subscriptions = new Subscription();
  }

  ngOnInit(): void {
    this.user = this.userService.user;
    this.view = this.store.pipe(select(state => state.activeRoute), map(route => route.split('/').pop()));
    this.credits = this.store.pipe(select(state => state.userCredits));
    this.subscriptions
      .add(this.creditService.getUserCredits(this.user.email).subscribe(value => this.store.dispatch(new Load(value.quantity))));
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  onChangeSelectedScreen(selectedScreen: string) {
    this.router.navigate(['home', selectedScreen]);
  }

  onOpenCreditsRequestModal() {
    fromPromise(this.modalService.open(CreditsRequestModalComponent, {centered: true}).result)
      .subscribe(value => value && value.credits ? this.onRequestCredits(value.credits) : '', error => console.error(error));
  }

  onGoBack() {
    this.location.back();
  }

  onLogout() {
    this.userService.removeUser();
    this.router.navigate(['login']);
  }

  onRequestCredits(amount: number) {
    this.loading = true;
    this.creditService.buyCredits({userEmail: this.user.email, quantity: amount})
      .pipe(finalize(() => this.loading = false))
      .subscribe(() => this.onSucessfullRequestCredits(amount));
  }

  onSucessfullRequestCredits(amount: number) {
    const ref = this.modalService.open(CreditsResponseModalComponent, {centered: true});
    (ref.componentInstance as CreditsResponseModalComponent).credits = amount;
    fromPromise(ref.result).subscribe(() => this.store.dispatch(new Buy(amount)));
  }
}
