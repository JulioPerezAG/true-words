import { Routes } from '@angular/router';

import { LayoutViewComponent } from './layout-view.component';

import { VerifyImageViewComponent } from '../verify-image-view/verify-image-view.component';
import { VerifyTextViewComponent } from '../verify-text-view/verify-text-view.component';
import { ActivityViewComponent } from '../activity-view/activity-view.component';
import { PublishNewsViewComponent } from '../publish-news-view/publish-news-view.component';

export const routes: Routes = [{
  path: '', component: LayoutViewComponent, children: [
    {path: 'verify-image', component: VerifyImageViewComponent},
    {path: 'verify-text', component: VerifyTextViewComponent},
    {path: 'publish-news', component: PublishNewsViewComponent},
    {path: 'activity', component: ActivityViewComponent},
    {path: '', pathMatch: 'full', redirectTo: 'verify-image'}
  ]
}];
