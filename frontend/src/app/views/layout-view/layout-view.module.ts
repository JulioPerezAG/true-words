import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

import { routes } from './layout-view.routes';

import { LayoutViewComponent } from './layout-view.component';

import { LayoutModule } from '../../components/layout/layout.module';

import { CreditsRequestModalModule } from '../../components/credits-request-modal/credits-request-modal.module';
import { CreditsResponseModalModule } from '../../components/credits-response-modal/credits-response-modal.module';

import { VerifyImageViewModule } from '../verify-image-view/verify-image-view.module';
import { VerifyTextViewModule } from '../verify-text-view/verify-text-view.module';
import { ActivityViewModule } from '../activity-view/activity-view.module';
import { PublishNewsViewModule } from '../publish-news-view/publish-news-view.module';

import { CurrentCreditService } from 'src/app/services/current-credit.service';
import { CreditService } from '../../services/credit.service';

@NgModule({
  declarations: [
    LayoutViewComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NgbModalModule,
    LayoutModule,
    VerifyImageViewModule,
    VerifyTextViewModule,
    PublishNewsViewModule,
    ActivityViewModule,
    CreditsRequestModalModule,
    CreditsResponseModalModule
  ],
  exports: [
    LayoutViewComponent
  ],
  providers: [
    CurrentCreditService,
    CreditService
  ]
})
export class LayoutViewModule {
}
