import { Routes } from '@angular/router';

import { LoginViewComponent } from './login-view.component';

export const routes: Routes = [
  {path: '', pathMatch: 'full', component: LoginViewComponent}];
