import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { finalize } from 'rxjs/operators';

import { AuthService } from '../../services/auth.service';
import { UserService } from '../../services/user.service';

import { LoginForm } from '../../models/login-form.interface';
import { LogUpForm } from '../../models/log-up-form.interface';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login-view',
  templateUrl: './login-view.component.html',
  styleUrls: ['./login-view.component.scss']
})
export class LoginViewComponent {

  formActivated: string;

  login = 'LOG_IN';

  logUp = 'LOG_UP';

  loading: boolean;

  constructor(private router: Router,
              private authService: AuthService,
              private userService: UserService,
              private toastrService: ToastrService) {
    this.formActivated = this.login;
  }

  onChangeFormActive(formActivated): void {
    this.formActivated = formActivated;
  }

  onAttemptLogin(loginForm: LoginForm) {
    this.loading = true;
    this.authService.login(loginForm)
      .pipe(finalize(() => this.loading = false))
      .subscribe(value => {
        this.userService.user = value;
        this.router.navigate(['home']);
      }, e => this.toastrService
        .error(e.error && e.error.message ? e.error.message : 'Ocurrio error mientras se intentaba iniciar sesión',
          'Por favor, intentelo de nuevo'));
  }

  onAttemptLogUp(logUpForm: LogUpForm) {
    this.loading = true;
    this.authService.logUp(logUpForm)
      .pipe(finalize(() => this.loading = false))
      .subscribe(value => {
        this.userService.user = value;
        this.router.navigate(['home']);
      }, e => this.toastrService
        .error(e.error && e.error.message ? e.error.message : 'Ocurrio error mientras se intentaba registrarlo',
          'Por favor, intentelo de nuevo'));
  }
}
