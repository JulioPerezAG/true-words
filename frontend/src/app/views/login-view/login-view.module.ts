import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { LoginViewComponent } from './login-view.component';

import { routes } from './login-view.routes';

import { LoginModule } from '../../components/login/login.module';
import { LogUpModule } from '../../components/log-up/log-up.module';

import { AuthService } from '../../services/auth.service';

@NgModule({
  declarations: [
    LoginViewComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    LoginModule,
    LogUpModule
  ],
  exports: [
    LoginViewComponent
  ],
  providers: [
    AuthService
  ]
})
export class LoginViewModule {
}
