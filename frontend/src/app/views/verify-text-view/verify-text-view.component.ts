import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';

import { Observable, Subscription } from 'rxjs';

import { select, Store } from '@ngrx/store';
import { FileCarousel } from '../../models/file-carousel.interface';
import { AppState } from '../../models/app-state.interface';

import * as ImagesActions from '../../actions/images.actions';
import * as SelectedImageActions from '../../actions/selected-image.actions';
import * as NewsActions from '../../actions/news.actions';


import { VerifyNewsService } from '../../services/verify-news.service';
import { VerifyService } from '../../services/verify.service';
import { ImageService } from '../../services/image.service';
import { UserService } from '../../services/user.service';

import { ToastrService } from 'ngx-toastr';

import { faCheckCircle } from '@fortawesome/free-solid-svg-icons/faCheckCircle';
import { News } from '../../models/news.interface';
import { NewsService } from '../../services/news.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-verify-text-view',
  templateUrl: './verify-text-view.component.html',
  styleUrls: [
    './verify-text-view.component.scss',
    '../../../assets/styles/true-words-forms.scss'
  ]
})
export class VerifyTextViewComponent implements OnInit, OnDestroy {

  images: Observable<FileCarousel[]>;

  loading: boolean;

  selectedImageIndex: number;

  form: FormGroup;

  subscriptions: Subscription;

  success = faCheckCircle;

  validNews: News;

  valid: boolean;

  constructor(formBuilder: FormBuilder,
              private verifyService: VerifyService,
              private verifyNewsService: VerifyNewsService,
              private userService: UserService,
              private imageService: ImageService,
              private store: Store<AppState>,
              private toastr: ToastrService,
              private newsService: NewsService) {
    this.form = formBuilder.group({
      text: ['', Validators.required],
      description: '',
      tags: ''
    });
    this.subscriptions = new Subscription();
  }

  ngOnInit(): void {
    this.images = this.store.pipe(select(state => state.images));
    this.form.valueChanges.subscribe(value => this.valid = _.isEqual(value, this.validNews));
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  onSave() {
    this.subscriptions.add(
      this.images.subscribe(value =>
        this.newsService.partialSave(this.userService.user.email, {
          text: this.form.value.text,
          description: this.form.value.description,
          images: value.map(value1 => value1.url),
          tags: this.form.value.tags
        }).subscribe(id => {
          this.toastr.success(`La noticia se ha guardado con exito`);
          this.store.dispatch(new NewsActions.LoadId(id));
          this.validNews = this.form.value;
        })));
  }

  onValidate() {
    this.subscriptions.add(
      this.images.subscribe(value => {
        if (value && value.length) {
          this.subscriptions.add(
            this.verifyNewsService.verifyNews({
              news: {
                text: this.form.value.text,
                description: this.form.value.description,
                images: value.map(value1 => value1.url),
                tags: this.form.value.tags
              },
              userEmail: this.userService.user.email
            }).subscribe(() => this.toastr.success(`La noticia se ha validado con exito`)
              ,
              (error: HttpErrorResponse) => {
                this.toastr.error(error.error && error.error.error
                  ? error.error.error : 'Ha surgido un error al intentar validar la noticia, por favor, intentalo de nuevo');
                console.log(error);
              }));
        }
      }));
  }

  onCancel() {
    this.resetView();
  }

  resetView() {
    this.form.reset();
    this.store.dispatch(new SelectedImageActions.UnSelect());
    this.store.dispatch(new ImagesActions.RemoveAll());
  }

  onDelete(item: number) {
    this.store.dispatch(new ImagesActions.Remove(item));
  }
}
