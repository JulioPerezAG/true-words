import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { VerifyTextViewComponent } from './verify-text-view.component';

import { EditorModule } from '../../components/editor/editor.module';
import { FileCarouselModule } from '../../components/file-carousel/file-carousel.module';
import { ChipsInputModule } from '../../components/chips-input/chips-input.module';
import { ConfirmModalModule } from '../../components/confirm-modal/confirm-modal.module';

import { VerifyNewsService } from '../../services/verify-news.service';

import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [
    VerifyTextViewComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    EditorModule,
    FileCarouselModule,
    ChipsInputModule,
    ConfirmModalModule,
    NgbModalModule,
    FontAwesomeModule
  ],
  exports: [
    VerifyTextViewComponent
  ],
  providers: [
    VerifyNewsService
  ]
})
export class VerifyTextViewModule {

}
