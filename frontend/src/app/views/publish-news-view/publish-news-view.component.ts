import { Component, OnInit } from '@angular/core';

import { select, Store } from '@ngrx/store';

import * as NewsActions from '../../actions/news.actions';
import * as ImageActions from '../../actions/images.actions';
import { Remove } from '../../actions/images.actions';

import { Observable, Subscription } from 'rxjs';
import { fromPromise } from 'rxjs/internal-compatibility';

import { News } from '../../models/news.interface';
import { AppState } from '../../models/app-state.interface';
import { FileCarousel } from '../../models/file-carousel.interface';

import { ConfirmModalComponent } from '../../components/confirm-modal/confirm-modal.component';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NewsService } from '../../services/news.service';
import { UserService } from '../../services/user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-publish-news-view',
  templateUrl: './publish-news-view.component.html'
})
export class PublishNewsViewComponent implements OnInit {

  images: Observable<FileCarousel[]>;

  news: Observable<News>;

  subscriptions: Subscription;

  email: string;

  constructor(private store: Store<AppState>,
              private modalService: NgbModal,
              private newsService: NewsService,
              private userService: UserService,
              private toast: ToastrService) {
    this.subscriptions = new Subscription();
  }

  ngOnInit(): void {
    this.images = this.store.pipe(select(state => state.images));
    this.news = this.store.pipe(select(state => state.news));
    this.email = this.userService.user.email;
  }

  onDelete(index: number) {
    this.store.dispatch(new Remove(index));
  }

  onPublishNews(news: News) {
    this.newsService.saveNews(this.email, news).subscribe(() => {
      this.toast.success('Noticia publicada con exito!');
      this.store.dispatch(new NewsActions.Clear());
      this.store.dispatch(new ImageActions.RemoveAll());
    });
  }

  onAttemptPublish(news: News) {
    this.subscriptions.add(fromPromise(this.modalService.open(ConfirmModalComponent, {centered: true}).result)
      .subscribe(() => this.onPublishNews(news), console.error));
  }

  onClear() {
    this.newsService.deletePartial(this.email).subscribe(() => {
      this.store.dispatch(new NewsActions.Clear());
      this.store.dispatch(new ImageActions.RemoveAll());
      this.toast.info('Se elimino la noticia');
    });
  }
}
