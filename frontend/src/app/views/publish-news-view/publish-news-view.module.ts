import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublishNewsViewComponent } from './publish-news-view.component';

import { PublishNewsModule } from '../../components/publish-news/publish-news.module';

@NgModule({
  declarations: [
    PublishNewsViewComponent
  ],
  imports: [
    CommonModule,
    PublishNewsModule
  ],
  exports: [
    PublishNewsViewComponent
  ]
})
export class PublishNewsViewModule {

}
