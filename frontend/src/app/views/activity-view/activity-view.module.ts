import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ActivityViewComponent } from './activity-view.component';
import { ActivityButtonModule } from '../../components/activity-button/activity-button.module';

@NgModule({
  declarations: [
    ActivityViewComponent
  ],
  imports: [
    CommonModule,
    ActivityButtonModule
  ],
  exports: [
    ActivityViewComponent
  ]
})
export class ActivityViewModule {

}
