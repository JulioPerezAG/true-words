import { NgModule } from '@angular/core';

import { LongClickDirective } from './long-click.directive';

@NgModule({
  declarations: [
    LongClickDirective
  ],
  imports: [],
  exports: [
    LongClickDirective
  ]
})
export class LongClickModule {

}
