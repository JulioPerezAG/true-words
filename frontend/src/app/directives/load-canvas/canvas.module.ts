import { NgModule } from '@angular/core';

import { CanvasDirective } from './canvas.directive';

@NgModule({
  declarations: [
    CanvasDirective
  ],
  imports: [],
  exports: [
    CanvasDirective
  ]
})
export class CanvasModule {

}
