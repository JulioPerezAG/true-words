var firebaseConnection = require('../common/firebase-connection');
var database = firebaseConnection.database();
var creditRef = database.ref('credits');
var creditService = {};

var reduceCredits = function (userEmail, quantity) {
    getUserCredits.then((data) => {
        console.log(data);
    });
}

var getUserCredits = function (userEmail) {
    return creditRef.orderByChild('userEmail').equalTo(userEmail)
        .once('value', (snapshot));
}

creditService.reduceCredits = reduceCredits;

module.exports = creditService;