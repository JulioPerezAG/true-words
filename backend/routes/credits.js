var express = require('express');
var creditsRouter = express.Router();
var httpStatusCodes = require('http-status-codes');
var firebaseConnection = require('../common/firebase-connection');
var creditsService = require('../services/credit.service');

creditsRouter.get('/testing', function(req, res){
    creditsService.reduceCredits('hugo110795@gmail.com');
    res.send();
})

creditsRouter.get('', function (req, res) {
    var userEmail = req.header('userEmail');

    if (!userEmail) {
        res.status(httpStatusCodes.BAD_REQUEST);
        res.json({ 'error': 'User email mandatory, please verify the data' });
    }

    var database = firebaseConnection.database().ref('credits');
    database.orderByChild('userEmail').equalTo(userEmail)
        .once('value', (snapshot) => {
            if (!snapshot.val())
                res.json({ 'quantity': 0 });

            snapshot.forEach((data) => {
                res.json({ quantity : data.val().quantity});
            })
        })
})

creditsRouter.put('/buy-credits', function (req, res) {
    var { quantity, userEmail } = req.body;

    if (!quantity || !userEmail) {
        res.status(httpStatusCodes.BAD_REQUEST);
        res.json({ 'error': 'Please verify the request data, Email and Quantity are mandatory' })
    }

    firebaseConnection.database()
        .ref('credits').orderByChild('userEmail').equalTo(userEmail)
        .once('value', (snapshot) => {
            if (snapshot.val()) {
                var rowKey = {};
                snapshot.forEach((data) => {
                    rowKey.data = data.val();
                    rowKey.key = data.key;
                });

                updateTotalCredits(req.body, rowKey, res);
            } else {
                updateTotalCredits(req.body, null, res);
            }
        })

});

var updateTotalCredits = function (credits, rowKey, res) {
    var database = firebaseConnection.database();
    if (!rowKey) {
        database.ref('credits').push(credits).then(() => {
            res.status(httpStatusCodes.OK);
            res.send();
        }, (err) => {
            res.status(httpStatusCodes.INTERNAL_SERVER_ERROR);
            res.send({ 'error': err });
        });
    } else {
        credits.quantity += rowKey.data.quantity;
        database.ref('credits/' + rowKey.key).update(credits).then(() => {
            res.status(httpStatusCodes.OK);
            res.send();
        }, (err) => {
            res.status(httpStatusCodes.INTERNAL_SERVER_ERROR);
            res.send({ 'error': err });
        });
    }

}

module.exports = creditsRouter;